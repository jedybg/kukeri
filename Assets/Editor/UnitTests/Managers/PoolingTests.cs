﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading;
//using Elarion.Managers;
//using NUnit.Framework;
//using UnityEngine;
//using Object = UnityEngine.Object;
//
//namespace Elarion.Tests {
//	[TestFixture]
//	public class PoolingTests {
//
//		[SetUp]
//		public void InitializePoolingManager() {
//			Assert.IsNull(Singleton.Get<PoolingManager>());
//			var gameObject = new GameObject();
//			gameObject.AddComponent<PoolingManager>();			
//		}
//
//		[TearDown]
//		public void DestroyPoolingManager() {
//			Assert.IsNotNull(Singleton.Get<PoolingManager>());
//			Object.DestroyImmediate(Singleton.Get<PoolingManager>().gameObject);
//			Assert.IsNull(Singleton.Get<PoolingManager>());
//		}
//
//		[Test]
//		[Category("Pooling a single instance")]
//		public void PoolOneTest() {
//			var original = new GameObject("Original");
//			try {
//				Pool pool = original.Pool(1);
//				Assert.AreEqual(pool.Count, 1);
//				pool.Clear();
//				Assert.AreEqual(pool.Count, 0);
//			} finally {
//				Object.DestroyImmediate(original);
//			}
//
//			//create a game object to pool
//			//add it to pool
//			//check if the pool size is 1
//
//
//			
//		}
//
//
//	}
//}
