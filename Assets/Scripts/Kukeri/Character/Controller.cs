﻿using System.Diagnostics;
using Elarion;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Kukeri.Character {
	public class Controller : ExtendedBehaviour {

		public Motor motor;

		public float movementSpeed = 20;
		public float jumpSpeed = 5;

		//if the drag value is below a threshold - we're not processing operations for it, it's most likely input imperfection
		public float jumpThreshold = 0.1f; 
		public float stopThreshold = 0.4f; 

		public void OnSwipe(Vector2 drag) {
			if(drag.y < -stopThreshold) {
				motor.Stop();
				return;
			}
			motor.Move(drag.x * movementSpeed);
			if(drag.y > jumpThreshold) 
				motor.Jump(drag.y * jumpSpeed);
		}
	}
}