﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Elarion;
using UnityEngine;
using Time = UnityEngine.Time;

namespace Kukeri.Character {
	[RequireComponent(typeof(Rigidbody2D))]
	public class Motor : ExtendedBehaviour {
		public MovementState movementState;

		private Rigidbody2D _rigidbody;
		private bool _grounded;
		private float _lockMovement;

		public void Awake() {
			_rigidbody = GetComponent<Rigidbody2D>();
		}

		public void LateUpdate() {
			if((_lockMovement -= Time.smoothDeltaTime) > 0) {
				return;
			}
			

			if(_grounded = _rigidbody.velocity.y == 0) {
				if(MovementState == MovementState.Airborne || MovementState == MovementState.Jumping) {
					MovementState = MovementState.Running;
				}
				if(_rigidbody.velocity.x == 0) {
					MovementState = MovementState.Idle;
				}
			}
		}

		public void Stop() {
			_rigidbody.AddForce(Vector2.right * -_rigidbody.velocity.x);
		}

		public void Move(float moveSpeed) {
			_rigidbody.AddForce(Vector2.right * moveSpeed);
			MovementState = MovementState.Running;
		}

		public void Jump(float jumpSpeed) {
			if(!_grounded) return;
			MovementState = MovementState.Jumping;
			_rigidbody.AddForce(Vector2.up * jumpSpeed);
			StartCoroutine(SwitchToAirborne());
		}

		private IEnumerator SwitchToAirborne() {
			yield return new WaitForSeconds(0.2f);
			if(MovementState == MovementState.Jumping)
				MovementState = MovementState.Airborne;
		}

		public MovementState MovementState {
			get { return movementState; }
			set {
				movementState = value;
				
				_lockMovement = 0.1f;
			}
		}
	}
}
