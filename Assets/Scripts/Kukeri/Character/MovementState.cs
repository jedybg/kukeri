﻿using System;

namespace Kukeri.Character {
	[Serializable]
	public enum MovementState {
		Idle,
		Running,
		Jumping,
		Airborne,
		Dying
	}
}