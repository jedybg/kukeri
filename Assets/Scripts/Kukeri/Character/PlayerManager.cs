﻿using System;
using Elarion;
using UnityEngine;

namespace Kukeri.Character {
	public class PlayerManager : Singleton {

		public Controller playerController;

		public UICamera uiCamera;

		public bool inGame;

		private Vector2 _dragStartPosition;

		new void Awake() {
			base.Awake();
			uiCamera = UICamera.FindCameraForLayer(gameObject.layer);
			UICamera.onDragStart += OnDragStart;
			UICamera.onDragEnd += OnDragEnd;

		}

		void Update() {
			if(!inGame) return;

		}

		private void OnDragStart(GameObject go) {
			_dragStartPosition = UICamera.lastTouchPosition;
		}

		private void OnDragEnd(GameObject go) {
			Vector2 drag = UICamera.lastTouchPosition - _dragStartPosition;
			drag.x = drag.x / Screen.width;
			drag.y = drag.y / Screen.height;
			playerController.OnSwipe(drag);
		}
	}
}