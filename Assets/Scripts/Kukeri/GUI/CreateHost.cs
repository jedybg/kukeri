﻿using UnityEngine;
using System.Collections;

public class CreateHost : MonoBehaviour {
	
	public GameObject playerPrefab;
	public GameObject playerSpawnPoint;

	void OnServerInitialized() {
		Debug.Log ("Successfully established host");
		SpawnPlayer ();
	}
	
	public void Create() {
		Debug.Log ("Init server");
		NetworkConnectionError res = Network.InitializeServer (32, 25000, !Network.HavePublicAddress ());
		Debug.Log ("Res: " + res.ToString ());
	}
	
	private void SpawnPlayer () {
		Network.Instantiate (playerPrefab, playerSpawnPoint.transform.position, Quaternion.identity, 0);
		Debug.Log ("Spawned player at world");
	}
}
