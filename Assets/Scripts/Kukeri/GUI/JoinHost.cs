﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Enemy {
	public GameObject prefab;
	public GameObject spawnPoint;
};

public class JoinHost : MonoBehaviour {
	
	public Enemy[] enemies;

	void OnConnectedToServer() {
		Debug.Log ("Connected to server");
		networkView.RPC ("SpawnObjects", RPCMode.All);
		Application.LoadLevel ("Game");
	}

	public void Join () {
		UpdateHostsList updScript = GameObject.Find ("Main Camera").GetComponent<UpdateHostsList>();

		Network.Connect (updScript.currentIP, updScript.currentPort);
		Debug.Log ("Joined host " + updScript.currentIP);
	}
	
	[RPC]
	public void SpawnObjects() {
		Debug.Log ("enemies " + enemies.Length);
		foreach (var enemy in enemies) {
			if (GameObject.Find (enemy.prefab.name) == null) {
				DontDestroyOnLoad(Network.Instantiate (enemy.prefab, enemy.spawnPoint.transform.position, Quaternion.identity, 0));
				Debug.Log ("Spawned enemy " + enemy.prefab.name);
				break;
			}
		}
	}
}
