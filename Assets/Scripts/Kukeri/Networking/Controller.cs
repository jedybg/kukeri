using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	private float lastSyncTime = 0.0f;
	private float syncDelay = 0.0f;
	private float syncTime = 0.0f;
	private Vector3 syncStartPosition = Vector3.zero;
	private Vector3 syncEndPosition = Vector3.zero;

	public float speed = 3.0f;
	public float levelBottom = -5.0f;

	void Start () {
		DontDestroyOnLoad(gameObject);
	}

	// Update is called once per frame
	void Update () {
		if (networkView.isMine) {
			Move ();
			CheckHasFallen();
		} else {
			SyncMovement();
			gameObject.renderer.enabled = false;
		}
	}

	private void Move() {
		syncStartPosition = rigidbody.position;
		if (Input.GetKey (KeyCode.RightArrow)) {
			rigidbody.MovePosition (rigidbody.position + Vector3.right * speed * Time.deltaTime);	
		}
		
		if (Input.GetKey (KeyCode.LeftArrow)) {
			rigidbody.MovePosition (rigidbody.position - Vector3.right * speed * Time.deltaTime);
		}
	}

	private void CheckHasFallen() {
		if (rigidbody.position.y < levelBottom) {
			StartAgain();
		}
	}

	private void SyncMovement() {
		syncTime += Time.deltaTime;
		rigidbody.position = Vector3.Lerp (syncStartPosition, syncEndPosition, syncTime / syncDelay);
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		Vector3 syncPosition = Vector3.zero;
		Vector3 syncVelocity = Vector3.zero;
		if (stream.isWriting) {
			syncPosition = rigidbody.position;
			stream.Serialize (ref syncPosition);

			syncVelocity = rigidbody.velocity;
			stream.Serialize (ref syncVelocity);
		} else {
			stream.Serialize (ref syncPosition);
			stream.Serialize (ref syncVelocity);

			syncTime = 0.0f;
			syncDelay = Time.time - lastSyncTime;
			lastSyncTime = Time.time;
			
			syncStartPosition = rigidbody.position;
			syncEndPosition = syncPosition + syncVelocity * syncDelay;
		}
	}

	public void StartAgain() {
		transform.position = GameObject.Find ("SpawnPlayer").transform.position;
	}
}
