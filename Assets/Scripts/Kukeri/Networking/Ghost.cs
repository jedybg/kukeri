﻿using UnityEngine;
using System.Collections;

public class Ghost : MonoBehaviour {

	public float delta = 0.1f;
	public float minX = -0.5f;
	public float maxX = 0.5f;

	// Update is called once per frame
	void Update () {
		if (transform.position.x > maxX || transform.position.x < minX) {
			delta *= -1;
		}
		transform.Translate (delta * Time.deltaTime, -0.0f, 0.0f);

		if (! networkView.isMine) {
			gameObject.renderer.enabled = false;
		}
	}
}
