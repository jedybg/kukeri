﻿using UnityEngine;
using System.Collections;

public class MovePlatform : MonoBehaviour {

	public float delta = 0.3f;
	public float rangeMinX = -1.3f;
	public float rangeMaxX = -0.9f;
	public float rangeMaxY = 1.2f;
	public float rangeMinY = 0.5f;

	// Update is called once per frame
	void Update () {
		if (transform.position.x > rangeMaxX || transform.position.x < rangeMinX ||
			transform.position.y > rangeMaxY || transform.position.y < rangeMinY) {
			delta *= -1;
		}
		transform.Translate (delta * Time.deltaTime, -delta * Time.deltaTime, 0.0f);

		if (! networkView.isMine) {
			gameObject.renderer.enabled = false;
		}
	}
}
