﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using SimpleJSON;

public class NetworkManager : MonoBehaviour {

	public String WebAPIurl = "http://128.199.48.184:5000/kukeri";

	
	public WWW GET_TEAMS(string url)
	{
		WWW www = new WWW (url);
		StartCoroutine (WaitForRequest (www));
		return www; 
	}
	
	public WWW POST_TEAMS(string url, string postData)
	{    
		Hashtable headers = new Hashtable();
		headers.Add("Content-Type", "application/json");
		
		byte[] pData = Encoding.ASCII.GetBytes(postData.ToCharArray());
		
		WWW www = new WWW(url, pData, headers);
		
		
		StartCoroutine(WaitForRequest(www));
		return www; 
	}
	
	private IEnumerator WaitForRequest(WWW www)
	{
		yield return www;
		
		
		// check for errors
		if (www.error == null)
		{
			Debug.Log("WWW Ok!: " + www.text);
		} else {
			Debug.Log("WWW Error: "+ www.error);
		}    
	}

	public void GotoGames() {
		Application.LoadLevel ("Games");
	}
}