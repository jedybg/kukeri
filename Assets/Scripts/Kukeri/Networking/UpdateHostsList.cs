﻿using UnityEngine;
using System.Collections;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using SimpleJSON;

public class UpdateHostsList : MonoBehaviour {

	public GameObject hostButton;

	public string currentIP;
	public int currentPort;

	public String WebAPIurl = "http://128.199.48.184:5000/kukeri";

	// Use this for initialization
	void Start () {
		GET_HOSTS(WebAPIurl + "/hosts");
	}
	
	// ---------- SERVER REQUESTS ---------------//
	public void GET_HOSTS(string url)
	{
		WWW www = new WWW (url);
		StartCoroutine (WaitForHosts (www));
	}
	
	public void POST_HOST(string url, string postData)
	{    
		Hashtable headers = new Hashtable();
		headers.Add("Content-Type", "application/json");
		
		byte[] pData = Encoding.ASCII.GetBytes(postData.ToCharArray());
		
		WWW www = new WWW(url, pData, headers);
	}

	private IEnumerator WaitForHosts(WWW www)
	{
		yield return www;

		// check for errors
		if (www.error == null)
		{
			Debug.Log("WWW Ok!: " + www.text);

			JSONArray hosts = JSON.Parse(www.text)["Hosts"].AsArray;
			for (int i = 0; i < hosts.Count; i++) {				
				currentIP = hosts[i]["ipadr"];
				currentPort = hosts[i]["port"].AsInt;
				break;
			}
		} else {
			Debug.Log("WWW Error: "+ www.error);
		}    
	}
	
	void addHost (string hostname, string ipadr, int port) 
	{
		string ourPostData = "{\"hostname\":\"" + hostname + "\"," +
			"\"ipadr\":\"" + ipadr + "\"" +
				",\"port\":" + port + "}";
		POST_HOST(WebAPIurl + "/hosts", ourPostData);
	}

	public string LocalIPAddress()
	{
		IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
		string localIP = "";
		foreach (IPAddress ip in host.AddressList)
		{
			if (ip.AddressFamily == AddressFamily.InterNetwork)
			{
				localIP = ip.ToString();
				break;
			}
		}
		return localIP;
	}
}